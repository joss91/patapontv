<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TokenRepository")
 */
class Token
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authToken;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="token", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $userRel;

    /**
     * @ORM\Column(type="datetime")
     */
    private $spawnTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthToken(): ?string
    {
        return $this->authToken;
    }

    public function setAuthToken(?string $authToken): self
    {
        $this->authToken = $authToken;

        return $this;
    }

    public function getUserRel(): ?User
    {
        return $this->userRel;
    }

    public function setUserRel(User $userRel): self
    {
        $this->userRel = $userRel;

        return $this;
    }

    public function getSpawnTime(): ?\DateTimeInterface
    {
        return $this->spawnTime;
    }

    public function setSpawnTime(\DateTimeInterface $spawnTime): self
    {
        $this->spawnTime = $spawnTime;

        return $this;
    }
}
