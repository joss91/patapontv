<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

    public $serializer;

    public function __construct(){
        $this->serializer = new Serializer([new JsonEncoder()], [new ObjectNormalizer()]);
    }

    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
     {   
       

        $content = $request->getContent();
        $content = json_decode($content, true);
        $hash = sha1(microtime(true).random_int(10000,90000));

        $user = new User();
        $user->setEmail($content["email"])
        ->setPatapon($content["patapon"])
        ->setUsername($content["username"])
        ->setPassword($content["password"])
        ->getToken()->setAuthToken($hash);

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        


     
    
        $em = $this->getDoctrine()->getManager();

        
        $em->persist($user);
        $em->flush();


        $response = new Response($serializer->serialize($user, 'json'));

        $response->headers->set('Content-Type', 'application/json');

        return $response;


    }

    /**
     * @Route("/login", name="log-in", methods={"POST"})
     */
    public function show(Request $request): Response
    {
        $content = $request->getContent();
        $data = json_decode($content, true);
        $username = $data['username'];
        $password = $data['password'];
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository(User::class);

     //   try {
            $user = $userRepository->findOneBy(
               ['username' => $username, 
               'password' => $password]
                 );
        // } catch (\Exception $e) {

        //     return new Response('error', 404);
        // }
            $spawnTime = $user->getToken()->getspawnTime();
            $day = 86400;
            $lifespawn = $spawnTime+$day;
            $now = new DateTime("now");

            if ($lifespawn > $now || $user->getToken()->getAuthToken() == null) {
                $token = sha1(microtime(true).random_int(10000,90000));
                $user->getToken()->setAuthToken($token);
                $em->persist($user);
                $em->flush();
            }
            
            
            

       
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);


        $response = new Response($serializer->serialize($user->getToken()->getAuthToken(), 'json'));

        $response->headers->set('Content-Type', 'application/json');

        return $response;
        
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index', [

                'id' => $user->getId(),
            ]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
