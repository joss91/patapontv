import { ConfigDb } from './../class/ConfigDb';
import { FilmDb } from './../class/movieDbFilm';
import { FilmService } from './../film.service';
import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.scss']
})
export class FilmDetailsComponent implements OnInit {

  id = null;
  film: FilmDb;
  config: ConfigDb;
  image: string;


  constructor(private route:ActivatedRoute, private filmService: FilmService) { }


  ngOnInit() {

     this.id = this.route.snapshot.params['id'];

     this.filmService.getConfig().subscribe(
       data => {
         this.config = data.images;
         console.log(this.config);
       }
     );

     this.filmService.getById(this.id).subscribe(data => {
      this.film = data;
      console.log(data);
      this.image = `${this.config.base_url}${this.config.backdrop_sizes[1]}${this.film.poster_path}`;

     });




  }

}
