import { FilmDb } from 'src/app/class/movieDbFilm';
import { Component, OnInit, Input } from '@angular/core';
import { ConfigDb } from '../class/ConfigDb';


@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent implements OnInit {

 @Input() film: FilmDb;
 @Input() config: ConfigDb;

  image: string;


 constructor() { }

 ngOnInit() {

  this.image = `${this.config.base_url}${this.config.backdrop_sizes[2]}${this.film.poster_path}`;
  console.log(this.image)
  }

}
