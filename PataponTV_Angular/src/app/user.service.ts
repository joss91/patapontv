import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {User} from './user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  addUrl = 'http://127.0.0.1:8000/user/new';
  getUrl = 'http://127.0.0.1:8000/user/login';




  addUser(entity: User): Observable <User> {
    return this.http.post<User>(this.addUrl, entity);
  }
  getUser(entity: User) {
    return this.http.post(this.getUrl, entity);
  }

  constructor(private http: HttpClient) { }
}
