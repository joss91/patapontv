import { Component, OnInit } from '@angular/core';
import {Film} from '../film';
import {FilmService} from '../film.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  films: Film[] = [];

  constructor(private filmService: FilmService) { }

  ngOnInit() {
    // this.filmService.getFilms()
    // .subscribe(data => this.films = data);
    // console.log(this.films);

  }


}
