import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { FilmDb } from 'src/app/class/movieDbFilm';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  // getAllFilms = 'http://127.0.0.1:8000/film/getall';
  // getId = 'http://127.0.0.1:8000/film/';

  // getFilms(): Observable <Film[]> {
  //   return this.http.get<Film[]>(this.getAllFilms);
  // }

  // getOneById(id: number) {
  //   return this.http.get<Film>(`${this.getId}+${id}`);
  // }

  ////////////////////////////////////////

  //   api themoviedb.org   //
  key = '?api_key=78c0b64e9048781e197774554f2f9bf6&language';
  api = 'https://api.themoviedb.org/3/';
  discoverMovies = 'discover/movie';
  config = 'configuration';
  lang = '=fr-FR';
  adult = false;
  video = true;
  sortBy = '&sort_by=popularity.desc';
  page = '1';

  getConfig() {
    return this.http.get(`
      ${this.api}${this.config}${this.key}
    `);
  }

  discover(): Observable<FilmDb[]> {

    return this.http.get<FilmDb[]>(`
      ${this.api}${this.discoverMovies}${this.key}${this.lang}&video=true${this.sortBy}&include_adult=${this.adult}&include_video=${this.video}&page=${this.page}
    `);
  }

  getById(id: number): Observable<FilmDb> {
    return this.http.get<FilmDb>(`
      ${this.api}movie/${id}${this.key}
      `);
  }


  constructor(private http: HttpClient) {

}
}
