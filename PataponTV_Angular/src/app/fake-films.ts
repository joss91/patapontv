import {Film} from './film';

export const Films: Film[] = [
  {id: 1, title: 'Casino', year: 1995, img: 'https://www.fillmurray.com/250/250', synopsys: 'lorem patapon blablabla chocolate cake'},
  {id: 2, title: 'Predestination', year: 2014, img: 'https://www.fillmurray.com/250/250', synopsys: 'lorem patapon blablabla chocolate cake'},
  {id: 3, title: 'Fight Club', year: 1999, img: 'https://www.fillmurray.com/250/250', synopsys: 'lorem patapon blablabla chocolate cake'},
  {id: 4, title: 'Snatch', year: 2000, img: 'https://www.fillmurray.com/250/250', synopsys: 'lorem patapon blablabla chocolate cake'},
  {id: 5, title: 'Layer Cake', year: 2004, img: 'https://www.fillmurray.com/250/250', synopsys: 'lorem patapon blablabla chocolate cake'},
  {id: 6, title: 'Les affranchis', year: 1990, img: 'https://www.fillmurray.com/250/250', synopsys: 'lorem patapon blablabla chocolate cake'}
];
