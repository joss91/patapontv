import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { User } from '../user';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  user = new User();

  logIn = new FormGroup({
    username: new FormControl(this.username),
    password: new FormControl(this.password)
  });

  onSubmit(){
    this.user.username = this.logIn.value.username;
    this.user.password = this.logIn.value.password;
    this.userService.getUser(this.user).subscribe(
      data  => localStorage.setItem('token', data.toString()));
}


  constructor(private userService: UserService) { }

  ngOnInit() {


  }

}
