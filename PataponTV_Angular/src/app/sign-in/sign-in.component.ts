import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';
import { User } from '../user';
import { FormControl, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  email: string;
  username: string;
  password: string;
  confirmPassword: string;
  patapon: boolean = false;

  toggle = false;


  user = new User();

  signIn = new FormGroup({
    email: new FormControl(this.email, [
      Validators.required,
      Validators.email
    ]),
    username: new FormControl(this.username),
    password: new FormControl(this.password),
    confirmPassword: new FormControl(this.confirmPassword),
    patapon: new FormControl(this.patapon)
  }
  );



  onSubmit() {

    if (this.signIn.value.password !== this.signIn.value.confirmPassword) {
      this.toggle = true;
    } else {
    this.toggle = false;
    this.user.email = this.signIn.value.email;
    this.user.username = this.signIn.value.username;
    this.user.password = this.signIn.value.password;
    this.user.patapon = this.signIn.value.patapon;
    console.log(this.user);
    this.userService.addUser(this.user)
    .subscribe(
      data  => {
      console.log(data);
      },
      error  => {

      console.log('Error', error);

      }

      );
    }
  }




  constructor(private userService: UserService) { }

  ngOnInit() {
  }

}
