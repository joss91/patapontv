import { ConfigDb } from './../class/ConfigDb';
import { FilmService } from './../film.service';
import { FilmDb } from 'src/app/class/movieDbFilm';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  config: ConfigDb;
  films: FilmDb[];
  constructor(private filmService: FilmService) { }

  ngOnInit() {
    this.filmService.getConfig()
    .subscribe(data => {
      this.config = data.images;
      console.log(this.config);
    });


    this.filmService.discover()
    .subscribe(data => {
      this.films = data.results;
      console.log(data);
      console.log(this.films);
      
    });
  }

}
